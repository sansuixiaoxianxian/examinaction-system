# 在线考试系统

#### 介绍

考试系统  
1.管理员可以建立若干的老师和学生帐户；  
2.每个老师登录后，可以出题，题目仅有选择题一种类型，且所有的题目都是单选题，老师录入题目的同时，还要录入正确答案，录入题目的过程中，老师随时可以按保存按钮保存， 除了录入题目，还有录入该考试的开始时间，结束时间；  
3.学生登录后，可以看到试卷的名称，但是没有到考试时间的时候，学生无法看到考卷内容；  
4.到达考试时间后，学生点开考卷，可以看到考卷内容，进行答题，并且必须在考试结束以前提及考卷，否则系统不再接受该同学提交的试卷；  
5.考试结束后，系统自动判分，按成绩高低排名；试卷分析：出卷老师登录系统后，可以看到该试卷参考人数，平均分，各分数人数分布， 每道题目回答的正确率；

#### 软件架构

基于 B/S 架构的小花在线考试系统的设计与实现，通过前后端分离技术(Vue)和 Tomcat 服务器搭建一个在线考试系统的设计与实现

#### 安装教程

| 前端快捷安装：前端开发基于 Vue+WebPack                                                                                                                                                                                                                             |
| :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1.需要先安装 node.js 环境,因为 webpack 依赖于 node.js，node.js 去[官网](https://nodejs.org/zh-cn/)上直接下载                                                                                                                                                       |
| 2.安装好了 node.js 后会有两个命令，可以使用 node -v 或 npm -v 查看是否安装成功和安装的命令版本 我的是 npm 是 6.14.7，nodejs 是 v12.10.0                                                                                                                            |
| 3.我们将使用 npm 命令进行 webpack 的安装，在安装之前 ，我们要写将 npm 命令进行一些操作，因为 npm 是一个下载器，他的下载都是在国外的资源库去下载的，所以会很慢，我们就要用 npm 下载一个叫 nrm 的命令，该命令只有一个作用，那就是更改 npm 命令的下载地址（镜像地址） |
| npm i nrm -g                                                                                                                                                                                                                                                       |
| nrm ls                                                                                                                                                                                                                                                             |
| npm -------- https://registry.npmjs.org/                                                                                                                                                                                                                           |
| yarn ------- https://registry.yarnpkg.com/                                                                                                                                                                                                                         |
| cnpm ------- http://r.cnpmjs.org/                                                                                                                                                                                                                                  |
| taobao ----- https://registry.npm.taobao.org/                                                                                                                                                                                                                      |
| nj --------- https://registry.nodejitsu.com/                                                                                                                                                                                                                       |
| npmMirror -- https://skimdb.npmjs.com/registry/                                                                                                                                                                                                                    |
| edunpm ----- http://registry.enpmjs.org/                                                                                                                                                                                                                           |
| nrm use 地址名称 （(nrm use taobao)                                                                                                                                                                                                                                |
| 5.以上都是运行环境搭建                                                                                                                                                                                                                                             |
| 6.下面将进行 webpack 的全局安装，必需要全局安装，不然在项目中就无法使用 webpack 的这个命令                                                                                                                                                                         |
| 7.使用上面安装 npm 命令进行 webpack 的全局安装 ==> npm i webpack@4.41.0 -g                                                                                                                                                                                         |

    * 注意 如果是这样安装的webpack那就将是最新版的webpack（4.x）以上 ，官方要求我们安装一个手脚架 webpack-cli  我们依然采用全局安装  => npm i webpack-cli@3.3.9 -g

| 完成以环境搭配后终端运行                                                                                                                                                                                                                                                                                                                                               |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 8.npm i webpack@4.44.1 css-loader@4.3.0 file-loader@6.1.0 html-webpack-plugin@4.5.0 node-sass@5.0.0 sass-loader@10.1.0 scss-loader@0.0.1 style-loader@1.2.1 url-loader@4.1.1 vue@2.6.10 vue-loader@15.9.5 vue-resource@1.5.1 vue-router@3.4.3 vue-template-compiler@2.6.10 webpack@4.44.1 webpack-cli@3.3.12 webpack-dev-server@3.11.0 vuex@3.1.1 vuex-along@1.2.11 -D |
| 9.npm i jquery@3.5.1 element-ui@2.14.1 localforage@1.9.0 xlsx@0.16.9 font-awesome@4.7.0 -S                                                                                                                                                                                                                                                                             |
| 10.终端运行 npm run dev                                                                                                                                                                                                                                                                                                                                                |

#### 概要设计

    系统功能图

![系统功能图](https://gitee.com/sansuixiaoxianxian/examinaction-system/raw/master/SystemImg/系统功能图.jpg)

    系统管理员用例图

![系统管理员用例图](https://gitee.com/sansuixiaoxianxian/examinaction-system/raw/master/SystemImg/管理员用例图.jpg)

    教师用例图

![教师用例图](https://gitee.com/sansuixiaoxianxian/examinaction-system/raw/master/SystemImg/教师用例图.jpg)

    学生用例图

![学生用例图](https://gitee.com/sansuixiaoxianxian/examinaction-system/raw/master/SystemImg/学生用例图.jpg)

#### 其它

VsCode 安装 Markdown Theme Kit 插件可实现 md 文件实时预览(ctrl+shift+v)

服务端：JDK1.8 + Springboot2.5 + Java + Mybatis 2.2.0+Redis2.6.  
前端：Vue+ Jquery +Webpck+ElementUI+npm.  
数据库：MySQL 8.0

#### 效果图

![](效果图/image1.png)

![](效果图/image2.png)

![](效果图/image3.png)
