// pages/bindInfo/bindInfo.js
//获取应用实例
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    eventChannel: '',
    stuName: '',
    stuId: '',
    userInfo: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      eventChannel: this.getOpenerEventChannel()
    })
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    let _this = this;
    this.data.eventChannel.on('acceptDataFromOpenerPage', function (data) {
      _this.setData({ userInfo: data.userInfo });
    });
  },

  cancel() {
    this.data.eventChannel.emit('acceptDataFromOpenedPage', { loginBtnIsShow: true });
    wx.navigateBack({
      delta: 1
    })
  },

  binding() {
    if (!this.checkInfo()) {
      wx.showToast({
        title: '用户信息有误！',
        icon: 'error',
        duration: 2000
      })
    }
    wx.request({
      url: app.baseUrl + '/wx/addUserInfo',
      method: 'POST',
      data: {
        'sessionToken': wx.getStorageSync('sessionToken').sessionToken,
        'stuId': this.data.stuId,
        'stuName': this.data.stuName,
        'nickName': this.data.userInfo.nickName,
        'avatarUrl': this.data.userInfo.avatarUrl
      },
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: res => {
        if (res.data == 'sessionToken Expired') {
          wx.removeStorageSync('sessionToken');
          this.data.eventChannel.emit('sessionTokenExpired', true);
          wx.showToast({
            title: '用户身份过期，请重试！',
            icon: 'error',
            duration: 2000
          })
          this.cancel();
        }
        if (res.data == 'user does not exist') {
          wx.showToast({
            title: '用户信息有误！',
            icon: 'error',
            duration: 2000
          })
        }
        if (res.data == 'success') {
          let sessionToken = wx.getStorageSync('sessionToken');
          sessionToken.userIsExists = true;
          wx.setStorageSync('sessionToken', sessionToken);
          wx.setStorageSync('userInfo', this.data.userInfo);
          this.data.eventChannel.emit('acceptDataFromOpenedPage', { loginBtnIsShow: false });
          let _this = this;
          wx.navigateTo({
            url: '../msgPage/msgPage',
            success: function (res) {
              // 通过eventChannel向被打开页面传送数据
              res.eventChannel.emit('stuInfo', { stuId: _this.data.stuId, stuName: _this.data.stuName });
            },
          })
        }
      }
    })
  },

  checkInfo() {
    let regId = /^\d{10}$/;
    let regName = /^[\u4e00-\u9fa5]{2,10}$/;
    if (regId.test(this.data.stuId) && regName.test(this.data.stuName)) return true;
    return false;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})