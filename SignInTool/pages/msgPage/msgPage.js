// pages/msgPage/msgPage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    returnTime: 5,
    stuId:'',
    stuName:'',
    eventChannel: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      eventChannel: this.getOpenerEventChannel()
    })
    
    let _this = this;

    this.data.eventChannel.on('stuInfo', function (data) {
      _this.setData({ 
        stuId: data.stuId,
        stuName: data.stuName
       });
    });

    let dt = setInterval(() => {
      let num = this.data.returnTime - 1;
      _this.setData({
        returnTime: num
      })
      if(_this.data.returnTime <= 1){
        clearInterval(dt)
        wx.navigateBack({
          delta: 2,
        })
        _this.data.eventChannel.emit('acceptDataFromOpenedPage', { loginBtnIsShow: false });
      }
    }, 1000);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})