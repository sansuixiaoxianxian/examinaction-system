// pages/index.js
//获取应用实例
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginBtnIsShow: false,
    // 签到完成提示框
    signInDialog: {
      visible: false,
      message: ''
    },
    // 签到加载框
    loadingDialog: {
      visible: false,
      message: "",
      hideLoading: false,
    },

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.checkLoginState();
    this.setLoginBtnIsShow();
  },

  setLoginBtnIsShow() {
    if (wx.getStorageSync('userInfo') == '')
      this.setData({ loginBtnIsShow: true })
    else
      this.setData({ loginBtnIsShow: false })
  },

  checkLoginState() {
    let sessionToken = wx.getStorageSync('sessionToken');
    if (sessionToken == '') {
      wx.removeStorageSync('userInfo');
      this.login().then(res => {
        if (res) { this.setLoginBtnIsShow(); return; };
      });
    }
    let expireTime = new Date(sessionToken.expireTime);
    if (new Date() > expireTime) {
      wx.removeStorageSync('userInfo');
      this.setLoginBtnIsShow();
      return;
    }
  },

  login() {
    return new Promise(resolve => {
      wx.login({
        timeout: 5000,
        success: res => {
          wx.request({
            url: app.baseUrl + '/wx/login',
            method: 'post',
            header: { 'content-type': 'application/x-www-form-urlencoded' },
            data: { 'code': res.code },
            success: res => {
              wx.setStorageSync('sessionToken', res.data);
              resolve(true);
            }
          })
        },
        fail: err => {
          wx.showToast({
            title: '访问错误，请稍后再试！',
            icon: 'none',
            duration: 2000
          })
          resolve(false);
        }
      })
    })
  },


  loginBtn() {
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        this.setData({
          loadingDialog: {
            visible: true,
            message: "登录中...",
            hideLoading: false,
          },
        })
        this.ifUserInfoIsExists(res.userInfo).then(res => {
          this.setData({
            loadingDialog: {
              visible: false,
              message: "",
              hideLoading: false,
            },
          })
        })
      }
    })
  },

  /**
   * 判断用户信息是否存在
   * 不存在则跳转到信息绑定页面
   * 若存在，则重新获取sessionToken
   */
  ifUserInfoIsExists(userInfo) {
    let _this = this;
    return new Promise(resolve => {
      if (wx.getStorageSync('sessionToken').userIsExists) {
        // 存在
        this.login().then(res => {
          if (res) {
            wx.setStorageSync('userInfo', userInfo);
            _this.setLoginBtnIsShow();
            resolve(true);
            return;
          };
        });
      } else {
        // 不存在
        wx.navigateTo({
          url: '../bindInfo/bindInfo',
          success: function (res) {
            // 通过eventChannel向被打开页面传送数据
            res.eventChannel.emit('acceptDataFromOpenerPage', { userInfo: userInfo });
            resolve(true);
          },
          events: {
            // 为指定事件添加一个监听器，获取被打开页面传送到当前页面的数据
            acceptDataFromOpenedPage: function (data) {
              _this.setData({ loginBtnIsShow: data.loginBtnIsShow })
            },
            sessionTokenExpired: function (data) {
              if (data) {
                _this.login();
              }
            }
          }
        })
      }
    })
  },

  /**
   * 扫码签到
   */
  signInBtn() {
    // 只允许从相机扫码
    let _this = this;
    wx.scanCode({
      onlyFromCamera: true,
      success(res) {
        _this.setData({
          loadingDialog: {
            visible: true,
            message: "签到中...",
            hideLoading: false,
          },
        })//打开加载框
        let sessionToken = {sessionToken: wx.getStorageSync('sessionToken').sessionToken, stuId: wx.getStorageSync('sessionToken').stuId};
        sessionToken = JSON.stringify(sessionToken);
        wx.request({
          url: app.baseUrl + '/wx/stuSignIn',
          method: "POST",
          header: { 'content-type': 'application/x-www-form-urlencoded' },
          data: { 'signInInfo': res.result, 'sessionToken': sessionToken },
          success: res => {
            _this.setData({//关闭加载框
              loadingDialog: {
                visible: false,
                message: "",
                hideLoading: false,
              },
            })
            switch (res.data) {
              case 43000:
                // 签到码错误
                _this.setSignInDialogMessage("签到码错误");
                break;
              case 43001:
                // 验证码过期
                _this.setSignInDialogMessage("签到码已过期");
                break;
              case 43002:
                // 重复签到
                _this.setSignInDialogMessage("你已签到成功");
                break;
              case 43003:
                // 用户令牌过期
                _this.setSignInDialogMessage("身份信息过期，请重新登录");
                break;
              case 43004:
                // 用户签到成功
                _this.setSignInDialogMessage("签到成功");
                break;
                case 43005:
                // 用户签到成功
                _this.setSignInDialogMessage("签到失败，请对应班级");
                break;
              default:
                break;
            }
          }
        })
      }
    })
  },

  setSignInDialogMessage(message){
    this.setData({
      signInDialog: {
        visible: true,
        message: message
      }
    })
  },

  /**
   * 关闭扫码后dialog弹框
   */
  closeSignInDialog() {
    this.setData({
      signInDialog: {
        visible: false,
        message: ""
      }
    })
  },







  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.setLoginBtnIsShow()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})