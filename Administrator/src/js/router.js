import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import manageClass from '../components/classManage/manageClass.vue';
import studentManage from '../components/studentManage/studentManage.vue';
import teacherManage from '../components/teacherManage/teacherManage.vue'



const router = new VueRouter({
  routes: [
    { path: '/', component: manageClass },
    { path: '/teacherManage', component: teacherManage }, ,
    { path: '/studentManage', component: studentManage },
  ]
})

export default router;