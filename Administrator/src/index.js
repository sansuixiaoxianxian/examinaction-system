import Vue from 'vue'
import app from './app.vue'
import './js/importImg.js'
import $ from 'jquery'
import ElementUI from 'element-ui'
Vue.use(ElementUI);
import 'element-ui/lib/theme-chalk/index.css'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.root = "http://localhost:8081"
Vue.prototype.$ROOT = "http://localhost:8081"
// Vue.http.options.root = "http://1b54bf64.nat3.nsloop.com"
// Vue.prototype.$ROOT = "http://1b54bf64.nat3.nsloop.com"
Vue.http.options.emulateJSON = true // 没有该方法，post提交后端获取不了
import router from './js/router.js'
import XLSX from 'xlsx'
Vue.prototype.$XLSX = XLSX;

const mv = new Vue({
    el: '#app',
    render: c => c(app),
    router,
})
