const path = require('path');
// 该插件用于解释单个vue组件使用
const { VueLoaderPlugin } = require('vue-loader')
// 导入在内存中生成页面的插件
const html = require('html-webpack-plugin')


module.exports = {
    mode: 'production', // 开发模式: development  ;  生产模式: production
    entry: {
        index: './src/index.js',
        element: './node_modules/element-ui/lib/element-ui.common.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            { test: /\.vue$/, use: 'vue-loader' },
            { test: /\.(sa|sc|c)ss$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
            { test: /\.(ttf|woff2|woff|eot|svg|otf|map)$/, use: 'url-loader' },
            {
                test: /\.(jpg|png|bmp|jpeg|gif|ico)$/, use: {
                    loader: 'file-loader',
                    options: {
                        limit: 10000,
                        name: '[name].[ext]',
                        outputPath: './images',
                        publicPath: './images'
                    }
                },
            },
        ]
    },
    optimization: {
        splitChunks: {
            chunks: "all",
            minSize: 30000,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            name: true,
            cacheGroups: {
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true,
                },
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                }
            }
        }
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.js',
        },
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    plugins: [
        new VueLoaderPlugin(),
        new html({
            // 创建一个在内存中生成html页面的插件
            template: path.join(__dirname, './src/index.html'), // 指定模块页面 
            filename: "index.html",
        }),
    ],
};
