/*
 Navicat Premium Data Transfer

 Source Server         : localhostDB
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : FloretExaminactionSystem

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 01/10/2021 17:57:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for 2006831
-- ----------------------------
DROP TABLE IF EXISTS `2006831`;
CREATE TABLE `2006831` (
  `studentName` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `studentNumber` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `studentPass` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `studentEmail` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`studentNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='软件工程1班';

-- ----------------------------
-- Records of 2006831
-- ----------------------------
BEGIN;
INSERT INTO `2006831` VALUES ('陈欢', '2006831501', '7abb0db9d36be9a40ce7564b88f47a9c', 'xxx@xxx.com');
INSERT INTO `2006831` VALUES ('王彦钧', '2006831514', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@xxx.com');
INSERT INTO `2006831` VALUES ('陈羽婷', '2006831520', 'c33367701511b4f6020ec61ded352059', 'xxx@xxx.com');
INSERT INTO `2006831` VALUES ('叶魏', '2006831526', '164d5fdfd02634293161afac4cf47299', 'xxx@xxx.com');
COMMIT;

-- ----------------------------
-- Table structure for classManage
-- ----------------------------
DROP TABLE IF EXISTS `classManage`;
CREATE TABLE `classManage` (
  `className` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级名称',
  `classNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级号',
  PRIMARY KEY (`classNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='班级目录';

-- ----------------------------
-- Records of classManage
-- ----------------------------
BEGIN;
INSERT INTO `classManage` VALUES ('软件工程1班', '2006831');
INSERT INTO `classManage` VALUES ('软件工程2班', '2006832');
COMMIT;

-- ----------------------------
-- Table structure for examinationRecord
-- ----------------------------
DROP TABLE IF EXISTS `examinationRecord`;
CREATE TABLE `examinationRecord` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `teacherNumber` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师工号',
  `classNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级号',
  `coursesName` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '课程名',
  `startTime` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开始时间',
  `endTime` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '结束时间',
  `questionBank` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '试题库',
  `studentPreview` tinyint(1) NOT NULL DEFAULT '1' COMMENT '学生试题分析权限',
  PRIMARY KEY (`id`),
  KEY `classNumber` (`classNumber`),
  KEY `teacherNumber` (`teacherNumber`),
  CONSTRAINT `examinationrecord_ibfk_1` FOREIGN KEY (`classNumber`) REFERENCES `classManage` (`classNumber`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `examinationrecord_ibfk_2` FOREIGN KEY (`teacherNumber`) REFERENCES `teacher` (`teacherNumber`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='考试记录';

-- ----------------------------
-- Table structure for questionBank
-- ----------------------------
DROP TABLE IF EXISTS `questionBank`;
CREATE TABLE `questionBank` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `questionName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '试题题目',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '题型',
  `score` int NOT NULL COMMENT '分值',
  `optionList` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '选项列表',
  `rightOption` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '正确选项',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='试题库模板';

-- ----------------------------
-- Table structure for signInRecord
-- ----------------------------
DROP TABLE IF EXISTS `signInRecord`;
CREATE TABLE `signInRecord` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `hashCode` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师编号和班级和课程名的哈稀值',
  `teacherNumber` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师编号',
  `classNumber` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级号',
  `courseName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '课程名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='签到记录';

-- ----------------------------
-- Records of signInRecord
-- ----------------------------
BEGIN;
INSERT INTO `signInRecord` VALUES (4, '387ec604e62f7f0097113acf846e5699', '2006831517', '2006831', '签到');
INSERT INTO `signInRecord` VALUES (5, '4c1cffaf1784b71469feb8a40576be0f', '2006831517', '2006831', '测试');
COMMIT;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `teacherName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师名',
  `teacherNumber` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师编号',
  `teacherPass` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `teacherEmail` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  PRIMARY KEY (`teacherNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='教师信息';

-- ----------------------------
-- Records of teacher
-- ----------------------------
BEGIN;
INSERT INTO `teacher` VALUES ('张健强', '2006831517', 'e10adc3949ba59abbe56e057f20f883e', '123');
COMMIT;

-- ----------------------------
-- Table structure for wx_user
-- ----------------------------
DROP TABLE IF EXISTS `wx_user`;
CREATE TABLE `wx_user` (
  `openId` varchar(28) NOT NULL COMMENT '用户唯一标识',
  `stuId` varchar(10) DEFAULT NULL COMMENT '学生学号',
  `stuName` varchar(10) DEFAULT NULL COMMENT '学生姓名',
  `nickName` varchar(16) DEFAULT NULL COMMENT '微信昵称',
  `avatarUrl` text COMMENT '微信头像',
  `gender` int DEFAULT NULL COMMENT '性别',
  `country` varchar(8) DEFAULT NULL COMMENT '国家',
  `province` varchar(20) DEFAULT NULL COMMENT '省份',
  `city` varchar(20) DEFAULT NULL COMMENT '城市',
  PRIMARY KEY (`openId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='微信用户';

-- ----------------------------
-- Records of wx_user
-- ----------------------------
BEGIN;
INSERT INTO `wx_user` VALUES ('o2MiL5Sa-LOp9Kc8bBYGnamqBBo0', '2006831501', '陈欢', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
