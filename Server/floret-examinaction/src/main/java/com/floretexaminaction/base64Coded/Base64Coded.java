package com.floretexaminaction.base64Coded;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

public class Base64Coded {

    /**
     * 解码
     * @param bytes bytes数组
     * @return string字符串
     */
    public static String decode(byte[] bytes){
        return new String(Base64.decodeBase64(bytes));
    }

    /**
     * 编码
     * @param bytes bytes数组
     * @return string字符串
     */
    public static String encode(byte[] bytes){
        return new String(Base64.encodeBase64(bytes));
    }

//    @Test
//    public void test(){
//        String str = "张健强";
//        System.out.println(encode(str.getBytes()));
//        System.out.println(decode(encode(str.getBytes()).getBytes()));
//    }
}
