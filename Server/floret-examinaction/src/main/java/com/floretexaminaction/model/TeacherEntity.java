package com.floretexaminaction.model;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "teacher")
@Table(name = "teacher", schema = "FloretExaminactionSystem", catalog = "")
public class TeacherEntity {
    private String teacherName;
    private String teacherNumber;
    private String teacherPass;
    private String teacherEmail;

    @Basic
    @Column(name = "teacherName", nullable = false, length = 10)
    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    @Id
    @Column(name = "teacherNumber", nullable = false, length = 10)
    public String getTeacherNumber() {
        return teacherNumber;
    }

    public void setTeacherNumber(String teacherNumber) {
        this.teacherNumber = teacherNumber;
    }

    @Basic
    @Column(name = "teacherPass", nullable = false, length = 20)
    public String getTeacherPass() {
        return teacherPass;
    }

    public void setTeacherPass(String teacherPass) {
        this.teacherPass = teacherPass;
    }

    @Basic
    @Column(name = "teacherEmail", nullable = false, length = 20)
    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeacherEntity that = (TeacherEntity) o;
        return Objects.equals(teacherName, that.teacherName) &&
                Objects.equals(teacherNumber, that.teacherNumber) &&
                Objects.equals(teacherPass, that.teacherPass) &&
                Objects.equals(teacherEmail, that.teacherEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacherName, teacherNumber, teacherPass, teacherEmail);
    }
}
