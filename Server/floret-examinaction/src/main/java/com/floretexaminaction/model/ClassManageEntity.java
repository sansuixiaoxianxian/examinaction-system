package com.floretexaminaction.model;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "classManage")
@Table(name = "classManage", schema = "FloretExaminactionSystem", catalog = "")
public class ClassManageEntity {
    private String className;
    private String classNumber;

    public ClassManageEntity() {
    }

    public ClassManageEntity(String className, String classNumber) {
        this.className = className;
        this.classNumber = classNumber;
    }

    @Basic
    @Column(name = "className", nullable = false, length = 30)
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Id
    @Column(name = "classNumber", nullable = false, length = 20)
    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassManageEntity that = (ClassManageEntity) o;
        return Objects.equals(className, that.className) &&
                Objects.equals(classNumber, that.classNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(className, classNumber);
    }
}
