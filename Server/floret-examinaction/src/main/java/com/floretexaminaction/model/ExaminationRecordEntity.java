package com.floretexaminaction.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity(name = "examinationRecord")
@Table(name = "examinationRecord", schema = "FloretExaminactionSystem", catalog = "")
public class ExaminationRecordEntity {
    private int id;
    private String teacherNumber;
    private String classNumber;
    private String coursesName;
    private String startTime;
    private String endTime;
    private String questionBank;

    public ExaminationRecordEntity() {
    }

    public ExaminationRecordEntity(int id, String teacherNumber, String classNumber, String coursesName, String startTime, String endTime) {
        this.id = id;
        this.teacherNumber = teacherNumber;
        this.classNumber = classNumber;
        this.coursesName = coursesName;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "teacherNumber", nullable = false, length = 10)
    public String getTeacherNumber() {
        return teacherNumber;
    }

    public void setTeacherNumber(String teacherNumber) {
        this.teacherNumber = teacherNumber;
    }

    @Basic
    @Column(name = "classNumber", nullable = false, length = 10)
    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    @Basic
    @Column(name = "coursesName", nullable = false, length = 30)
    public String getCoursesName() {
        return coursesName;
    }

    public void setCoursesName(String coursesName) {
        this.coursesName = coursesName;
    }

    @Basic
    @Column(name = "startTime", nullable = false, length = 20)
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "endTime", nullable = false, length = 20)
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExaminationRecordEntity that = (ExaminationRecordEntity) o;
        return id == that.id &&
                Objects.equals(teacherNumber, that.teacherNumber) &&
                Objects.equals(classNumber, that.classNumber) &&
                Objects.equals(coursesName, that.coursesName) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime);
    }

    @Override
    public String toString() {
        return "ExaminationRecordEntity{" +
                "id=" + id +
                ", teacherNumber='" + teacherNumber + '\'' +
                ", classNumber='" + classNumber + '\'' +
                ", coursesName='" + coursesName + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", questionBank='" + questionBank + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, teacherNumber, classNumber, coursesName, startTime, endTime);
    }

    @Basic
    @Column(name = "questionBank", nullable = true, length = 10)
    public String getQuestionBank() {
        return questionBank;
    }

    public void setQuestionBank(String questionBank) {
        this.questionBank = questionBank;
    }
}
