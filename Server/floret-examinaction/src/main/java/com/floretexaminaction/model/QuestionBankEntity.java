package com.floretexaminaction.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Table(name = "questionBank", schema = "FloretExaminactionSystem", catalog = "")
public class QuestionBankEntity {
    private int id;
    private String questionName;
    private String type;
    private int score;
    private String optionList;
    private String rightOption;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "questionName", nullable = false, length = 200)
    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 10)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "score", nullable = false)
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Basic
    @Column(name = "optionList", nullable = false, length = 300)
    public String getOptionList() {
        return optionList;
    }

    public void setOptionList(String optionList) {
        this.optionList = optionList;
    }

    @Basic
    @Column(name = "rightOption", nullable = false, length = 300)
    public String getRightOption() {
        return rightOption;
    }

    public void setRightOption(String rightOption) {
        this.rightOption = rightOption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionBankEntity that = (QuestionBankEntity) o;
        return id == that.id &&
                score == that.score &&
                Objects.equals(questionName, that.questionName) &&
                Objects.equals(type, that.type) &&
                Objects.equals(optionList, that.optionList) &&
                Objects.equals(rightOption, that.rightOption);
    }

    @Override
    public String toString() {
        return "QuestionBankEntity{" +
                "id=" + id +
                ", questionName='" + questionName + '\'' +
                ", type='" + type + '\'' +
                ", score=" + score +
                ", optionList='" + optionList + '\'' +
                ", rightOption='" + rightOption + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, questionName, type, score, optionList, rightOption);
    }
}
