package com.floretexaminaction.model;

public class StudentTestAnswersEntity {
    /**
     * let obj = {
     *     classNumber: this.$store.getters.getUserNumber.substring(0, 7),
     *     studentNumber: this.$store.getters.getUserNumber,
     *     questionBank: this.$store.getters.getStartExaminationNumber,
     *     rightOption: rightOption,
     * }
     */
    private String classNumber;
    private String studentNumber;
    private String questionBank;
    private String rightOption;

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getQuestionBank() {
        return questionBank;
    }

    public void setQuestionBank(String questionBank) {
        this.questionBank = questionBank;
    }

    public String getRightOption() {
        return rightOption;
    }

    public void setRightOption(String rightOption) {
        this.rightOption = rightOption;
    }

    @Override
    public String toString() {
        return "StudentTestAnswersEntity{" +
                "classNumber='" + classNumber + '\'' +
                ", studentNumber='" + studentNumber + '\'' +
                ", questionBank='" + questionBank + '\'' +
                ", rightOption='" + rightOption + '\'' +
                '}';
    }
}
