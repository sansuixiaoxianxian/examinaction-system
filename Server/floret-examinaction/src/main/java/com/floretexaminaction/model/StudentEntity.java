package com.floretexaminaction.model;

public class StudentEntity {
    private String studentName;
    private String studentNumber;
    private String studentPass;
    private String studentEmail;

    public StudentEntity() {
    }

    public StudentEntity(String studentName, String studentNumber, String studentPass, String studentEmail) {
        this.studentName = studentName;
        this.studentNumber = studentNumber;
        this.studentPass = studentPass;
        this.studentEmail = studentEmail;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getStudentPass() {
        return studentPass;
    }

    public void setStudentPass(String studentPass) {
        this.studentPass = studentPass;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    @Override
    public String toString() {
        return "StudentEntity{" +
                "studentName='" + studentName + '\'' +
                ", studentNumber='" + studentNumber + '\'' +
                ", studentPass='" + studentPass + '\'' +
                ", studentEmail='" + studentEmail + '\'' +
                '}';
    }
}
