package com.floretexaminaction.model;

public class StudentAnswerBankEntity {
    private String studentNumber;
    private String rightOption;

    public StudentAnswerBankEntity() {
    }

    public StudentAnswerBankEntity(String studentNumber, String rightOption) {
        this.studentNumber = studentNumber;
        this.rightOption = rightOption;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getRightOption() {
        return rightOption;
    }

    public void setRightOption(String rightOption) {
        this.rightOption = rightOption;
    }
}
