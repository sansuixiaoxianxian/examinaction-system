package com.floretexaminaction.dao;

import com.floretexaminaction.model.TeacherEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface TeacherDao extends CrudRepository<TeacherEntity, String> {
}
