package com.floretexaminaction.dao;

import com.floretexaminaction.model.ClassManageEntity;
import org.springframework.data.repository.CrudRepository;

public interface ClassManageDao extends CrudRepository<ClassManageEntity, String> {
}
