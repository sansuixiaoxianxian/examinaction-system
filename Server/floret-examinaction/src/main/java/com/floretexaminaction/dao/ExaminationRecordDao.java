package com.floretexaminaction.dao;

import com.floretexaminaction.model.ExaminationRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ExaminationRecordDao extends JpaRepository<ExaminationRecordEntity, Integer> {

    /**
     * 根据班级号，查询本班未开考的记录
     * @param className
     * @return
     */
   @Query(value = "select e.coursesName, e.startTime, e.endTime from examinationRecord e where e.startTime >= now() and e.classNumber = :className", nativeQuery = true)
    List<String> findStudentExamination(String className);
}
