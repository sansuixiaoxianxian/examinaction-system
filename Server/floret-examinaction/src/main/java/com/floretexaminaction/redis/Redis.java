package com.floretexaminaction.redis;
import redis.clients.jedis.Jedis;

public class Redis {
    private static Jedis redis = null;
    private Redis(){};
    public static Jedis getRedis(){
        if(redis == null){
            synchronized(Redis.class){
                if (redis == null){
                    redis = new Jedis("127.0.0.1", 6379);
                }
            }
        }
        return redis;
    }
}
