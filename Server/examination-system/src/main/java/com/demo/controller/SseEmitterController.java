package com.demo.controller;

import com.demo.util.SseEmitterServer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@CrossOrigin
@RestController
@RequestMapping("/sse")
public class SseEmitterController {
    private Log log = LogFactory.getLog("三岁小仙仙");

    /**
     * 用于创建连接（将用户注册到server中）
     */
    @GetMapping(value = "/connect/{userId}", produces = {MediaType.TEXT_EVENT_STREAM_VALUE})
    private SseEmitter connect(@PathVariable String userId) {
        log.info(userId);
        return SseEmitterServer.connect(userId);
    }
}
