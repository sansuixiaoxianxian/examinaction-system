package com.demo.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface SignInMapper {
    Map checkWxUserIsExists(String openId);

    int addOpenId(String openId, String stuId, String stuName, String nickName, String avatarUrl);

    Map checkSignInRecordIsExists(String hashCode);

    int addSignInRecord(String hashCode, String teacherNumber, String classNumber, String courseName);
}
