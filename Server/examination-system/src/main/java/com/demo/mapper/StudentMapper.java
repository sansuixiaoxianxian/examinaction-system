package com.demo.mapper;

import com.demo.entity.StudentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper
public interface StudentMapper {
    public StudentEntity findClassStudent(Map<String, String> map);

    public Map checkClassStuIsExists(String stuClass, String stuId, String stuName);

    public Map<String, Object> findClassAllStudent(Map<String, String> map);

    public List<Map<String, String>> getAnswerLibrary(String classNumber, String questionBank, String studentNumber);

    public List<Map<String, Object>> findClassCourseExaminationTime(@Param("classNumber") String classNumber, @Param("set") Set<String> strings);

    //不同类型的多参数传递，使用注解最简单。可封装一个在map或list中但开销大
    public void createStudentAnswerBank(@Param("key") String key, @Param("list") List<Map<String, String>> list);

    public List<Map<String, String>> getAnswersToQuestionBank(String questionBank);

    public void updateStudentExaminationResult(@Param("class_course") Map<String, String> class_course, @Param("list") List<Map<String, String>> list);

    public boolean updateStudentPass(String studentClass, String studentNumber, String studentPass);
}
