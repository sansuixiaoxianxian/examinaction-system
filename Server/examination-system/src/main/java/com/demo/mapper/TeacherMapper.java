package com.demo.mapper;

import com.demo.entity.ExaminationRecordEntity;
import com.demo.entity.QuestionBankEntity;
import com.demo.entity.TeacherEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 数据接口层，直接与数据库交互。（同时也是MyBatis的映射器组成之一，还有一个XML文件）
 * 只负责数据读写
 */
@Mapper
public interface TeacherMapper {
    /**
     * 根据教师编号，查询教师表中的教师信息
     *
     * @param userid 教师编号
     * @return TeacherEntity
     */
    TeacherEntity findById(String userid);

    /**
     * 根据教师编号 查询当前教师所有创建的考试记录
     *
     * @param teacherNumber 教师编号
     * @return List<Map < String, String>>
     */
    List<Map<String, String>> findTeacherCreateExamination(String teacherNumber);

    int countClassPeopleNumber(String classNumber);

    int deleteExamination(String teacherNumber, String classNumber, String coursesName);

    int deleteCourse(@Param("classNumber") String classNumber, @Param("courseName") String courseName);

    Object checkCountExaminationRecordIsNull();

    int countExaminationRecord();

    int deleteTable(String tableName);

    List<Map<String, String>> getAllClassList(String tableName);

    int addCourse(@Param("classNumber") String classNumber, @Param("courseName") String courseName);

    int createCoursesClass(ExaminationRecordEntity recordEntity);

    boolean updateCoursesClass(ExaminationRecordEntity recordEntity);

    ExaminationRecordEntity queryExaminationRecordById(String id);

    int createQuestionBank(String questionBank);

    //多不同类型参数且动态表名，则 接口传参使用@Param xml不需要parameterType 需要statementType='STATEMENT'
    boolean addQuestionList(@Param("questionBank") String questionBank, @Param("list") List<QuestionBankEntity> list);

    /**
     * 删除试题库所有数据
     * ---可公用---
     * 实际是根据表名，删除表中所有数据
     *
     * @param questionBank 试题库号
     */
    boolean deleteQuestionBank(String questionBank);

    /**
     * 查询班级某一科目的所有学生成绩
     *
     * @param classNumber 班级号
     * @param courseName  课程名（科目）
     * @return List<Map < String, String>>
     */
    List<Map<String, String>> autoGetClassStudentCoursesResult(String classNumber, String courseName);

    /**
     * 编辑考试记录信息
     *
     * @param teacherNumber 教师工号
     * @param classNumber   班级号
     * @param coursesName   课程名
     * @param startTime     更新的开始的时间
     * @param endTime       更新的结束时间
     * @return int
     */
    int editExamination(String teacherNumber, String classNumber, String coursesName, String startTime, String endTime);

    boolean updateTeacherPass(String teacherNumber, String newPass);

    /**
     * 更新学生试题分析查看权限
     *
     * @param id           试题库编号
     * @param questionBank 试题库号
     * @param status       更新状态
     * @return true/false
     */
    boolean upStudentPreview(String id, String questionBank, boolean status);

    /**
     * 模糊查找班级
     *
     * @param className 班级名称
     * @return 班级列表
     */
    List<Map> searchClass(String className);

    /**
     * 查询签到记录中的课程名
     *
     * @param classNumber   班级号
     * @param teacherNumber 教师编号
     * @return list
     */
    List<Map> queryClassSignInRecordCourseName(String classNumber, String teacherNumber);
}
