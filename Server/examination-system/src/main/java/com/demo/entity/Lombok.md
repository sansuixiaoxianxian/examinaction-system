/**
 * Lombok 使用教程
 * https://www.jianshu.com/p/365ea41b3573
 *
 * Data：注解在类上；提供类所有属性的getting和setting方法，此外还提供了equals、canEqual、hashCode 、toString 方法
 * @Setter：注解在属性上；为属性提供 setting 方法
 * @Getter：注解在属性上；为属性提供 getting 方法
 * @Slf4j：注解在类上；为类  提供一个属性名为log 的slf4j日志对象
 * @NoArgsConstructor：注解在类上：为类提供一个无参的构造方法
 * @AllArgsConstructor ：注解在类上；为类提供一个全参的构造方法
 * @NonNull：注解在参数上；如果该参数为 null 会 throw new NullPointerException(参数名);
 * @Cleanup：注释在引用变量前，自动回收资源 默认调用 close 方法
 * @SneakyThrows ：注解在方法上，为方法抛出指定异常
 */