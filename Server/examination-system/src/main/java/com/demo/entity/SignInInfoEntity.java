package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignInInfoEntity {
    //班级号
    private String classNumber;
    //教师编号
    private String teacherNumber;
    //签到码
    private String signInCode;
    //签到唯一凭证
    private String signInId;
    //签到结束时间
    private String signInEndTime;
    //课程名称
    private String courseName;
    //信息盖章
    private String infoSeal;
}
