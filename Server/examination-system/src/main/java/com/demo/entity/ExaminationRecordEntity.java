package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExaminationRecordEntity {
    private int id;
    private String teacherNumber;
    private String classNumber;
    private String coursesName;
    private String startTime;
    private String endTime;
    private String questionBank;
    private boolean studentPreview;
}
