package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class QuestionBankEntity {
    private int id;
    private String questionName;
    private String type;
    private int score;
    private String optionList;
    private String rightOption;
}
