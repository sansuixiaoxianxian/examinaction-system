package com.demo.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherEntity {
  private String teacherName;
  private String teacherNumber;
  private String teacherPass;
  private String teacherEmail;
}
