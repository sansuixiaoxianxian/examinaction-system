package com.demo.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class WebConfigurer implements WebMvcConfigurer {

    @Resource
    Interceptor interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String> path = new ArrayList<>();
        path.add("/getCode");
        path.add("/login");
        path.add("/getPubKey");
        path.add("/wx/*");
        path.add("/sse/*");
        registry.addInterceptor(interceptor).addPathPatterns("/**")
                .excludePathPatterns(path);
    }
}
