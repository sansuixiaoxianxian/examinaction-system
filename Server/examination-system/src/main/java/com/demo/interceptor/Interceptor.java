package com.demo.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.demo.token.TokenModel;
import com.demo.util.Redis;
import com.demo.util.ReturnErrorCode;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * preHandle：在业务处理器处理请求之前被调用。预处理，可以进行编码、安全控制、权限校验等处理；
 * postHandle：在业务处理器处理请求执行完成后，生成视图之前执行。后处理（调用了Service并返回ModelAndView，但未进行页面渲染），有机会修改ModelAndView （这个博主就基本不怎么用了）；
 * afterCompletion：在DispatcherServlet完全处理完请求后被调用，可用于清理资源等。返回处理（已经渲染了页面）；
 * ————————————————
 * 版权声明：本文为CSDN博主「韭韭韭韭菜」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/zhibo_lv/article/details/81699360
 */
@Component
public class Interceptor implements HandlerInterceptor {

    Jedis redis = Redis.getRedis();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestHeader = request.getHeader("TOKEN");

        if (StringUtils.isBlank(requestHeader) || requestHeader == "null") return false;

        TokenModel tokenModel = JSONObject.parseObject(requestHeader, TokenModel.class);
        if (redis.exists(tokenModel.getId()) && redis.get(tokenModel.getId()).equals(tokenModel.getToken())) {
            return true;
        }
        //token存在 但 token内容匹配错误
        if (redis.exists(tokenModel.getId())) redis.del(tokenModel.getId());
        new ReturnErrorCode(response, 401);
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception { }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception { }
}
