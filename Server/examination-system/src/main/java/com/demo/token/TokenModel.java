package com.demo.token;

public class TokenModel {
    String id;
    String token;

    public TokenModel() {
    }

    public TokenModel(String id, String token) {
        this.id = id;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "TokenModel{" +
                "id='" + id + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
