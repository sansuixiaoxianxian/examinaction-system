package com.demo.token;

import com.demo.util.Redis;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import java.util.UUID;

@Component
public class Token implements TokenHelper {

    Jedis redis = Redis.getRedis();

    @Override
    public TokenModel create(String id) {
        String token = UUID.randomUUID().toString();
        TokenModel tokenModel = new TokenModel(id, token);
        redis.set(id, token);
        redis.expire(id, 60*60*24);
        return tokenModel;
    }

    @Override
    public TokenModel create(String id, int expireTime) {
        String token = UUID.randomUUID().toString();
        TokenModel tokenModel = new TokenModel(id, token);
        redis.set(id, token);
        redis.expire(id, expireTime);
        return tokenModel;
    }

    @Override
    public TokenModel create(String id, String token, int expireTime) {
        TokenModel tokenModel = new TokenModel(id, token);
        redis.set(id, token);
        redis.expire(id, expireTime);
        return tokenModel;
    }

    @Override
    public boolean check(TokenModel tokenModel) {
        boolean result = false;
        if(tokenModel != null){
            String id = tokenModel.getId();
            String token = tokenModel.getToken();
            if (redis.exists(id)){
                if(redis.get(id).equals(token)){
                    result = true;
                }
            }
        }
        return result;
    }

    @Override
    public TokenModel get(String id) {
        return null;
    }

    @Override
    public boolean delete(String id) {
        System.out.println(redis.del(id));
        return true;
    }
}
