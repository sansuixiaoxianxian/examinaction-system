package com.demo.token;

public interface TokenHelper {
    public TokenModel create(String id);
    public TokenModel create(String id, int expireTime);
    public TokenModel create(String id, String token,int expireTime);
    public boolean check(TokenModel tokenModel);
    public TokenModel get(String id);
    public boolean delete(String id);
}
