package com.demo.util;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ReturnErrorCode {
    /**
     * 返回页面错误码
     * @param response 服务响应器
     * @param code     错误码
     *                 401：用户登录信息过期
     *                 402：用户登录账号锁定
     */
    public ReturnErrorCode(HttpServletResponse response, int code) {
        response.setContentType("application/json; charset=utf-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            writer.print(code);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
    }
}
