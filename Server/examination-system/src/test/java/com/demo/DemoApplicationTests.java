package com.demo;

import com.demo.mapper.SignInMapper;
import com.demo.mapper.StudentMapper;
import com.demo.mapper.TeacherMapper;
import com.demo.util.Redis;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;

@SpringBootTest
class DemoApplicationTests {

    private Logger log = LoggerFactory.getLogger(DemoApplicationTests.class);
    @Resource
    StudentMapper studentMapper;

    @Resource
    TeacherMapper teacherMapper;

    @Resource
    SignInMapper signInMapper;

    Jedis redis = Redis.getRedis();

    private static final String APPID = "wx9b04d88d2bf7f251";

    private static final String SESSIONKEY = "o2MiL5Sa-LOp9Kc8bBYGnamqBBo0";

    @Test
    void contextLoads() {
        log.info( redis.exists("UserBalance").toString() );
        log.info( redis.hkeys("UserBalance").toString() );
    }

}
