package com.demo.mapper;

import com.demo.filter.StudentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AdminMapper {
    public List<Map<String, String>> queryTableAllInfo(String tableName);

    public int addStudents(@Param("classNumber") String classNumber, @Param("list") List<Map> list);

    public int delStudents(@Param("classNumber") String classNumber, @Param("list") List<String> list);

    public int upStudents(@Param("classNumber") String classNumber, @Param("list") List<Map> list);

    public boolean delClassManageInfo(String classNumber);

    public void addClassManageInfo(String classNumber, String className);

    public boolean createClassTable(String classNumber, String className);
}
