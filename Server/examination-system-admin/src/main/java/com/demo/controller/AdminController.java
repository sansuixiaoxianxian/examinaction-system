package com.demo.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.demo.mapper.AdminMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

@RestController
public class AdminController {

    @Resource
    AdminMapper adminMapper;


    Log log = LogFactory.getLog("三岁小仙仙");

    /**
     * 获取班级列表
     *
     * @return json班级列表
     */
    @GetMapping("/adminGetClassList")
    private Object adminGetClassList() {
        List<Map<String, String>> classManage = adminMapper.queryTableAllInfo("classManage");
        return JSON.toJSON(classManage);
    }

    /**
     * 根据班级号获取本班学生信息 班级管理中的 查
     *
     * @param classNumber 班级号
     * @return 学生列表
     */
    @PostMapping("/queryUsers")
    private Object queryUsers(String classNumber, int pageNum, int pageSize) {
        try {
            PageHelper.startPage(pageNum, pageSize);
            List<Map<String, String>> allClassStudent = adminMapper.queryTableAllInfo(classNumber);
            PageInfo pageInfo = new PageInfo(allClassStudent);
            return JSON.toJSON(pageInfo);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 添加学生 班级管理中的 增
     *
     * @param classNumber 班级号
     * @param updateArr   数组对象
     * @return 添加修改数
     */
    @PostMapping("/addUsers")
    private int addUsers(HttpServletResponse response, String classNumber, String updateArr) {
        try {
            log.info(classNumber);
            List<Map> studentEntities = JSONArray.parseArray(updateArr, Map.class);
            log.info(studentEntities);
            return adminMapper.addStudents(classNumber, studentEntities);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 根据班级号和学号删除指定的学生信息  班级管理中的 删
     *
     * @param studentNumber 学号-班级号
     * @return ""
     */
    @PostMapping("/delUsers")
    private int delUsers(String classNumber, String studentNumber) {
        try {
            JSONArray jsonArray = JSONArray.parseArray(studentNumber);
            if (jsonArray.size() != 0) return adminMapper.delStudents(classNumber, jsonArray.toJavaList(String.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 更新学生信息 班级管理员的 改
     *
     * @param classNumber
     * @param data
     * @return
     */
    @PostMapping("/upUsers")
    private int upUsers(String classNumber, String data) {
        try {
            List<Map> list = JSON.parseArray(data, Map.class);
            return adminMapper.upStudents(classNumber, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 下载教师excel模板
     *
     * @param response
     * @throws IOException
     * @type 下载模板类型 student/teacher
     */
    @GetMapping("/downloadTemplate")
    private void downloadTemplate(HttpServletResponse response, String type) {
        File file = null;
        try {
            /**
             * 1、获取要下载的文件路径
             */
            URI uri = null;
            if (type.equals("student")) {
                uri = new ClassPathResource("static/downloadTemplate/studentTemplate.xlsx").getURI();
                /**
                 * 2、将获取文件路径加载到File流中
                 */
                file = new File(uri);
            } else if (type.equals("teacher")) {
                uri = new ClassPathResource("static/downloadTemplate/teacherTemplate.xlsx").getURI();
                file = new File(uri);
            } else {
                retFileExists(response);
            }

            /**
             * 3、获取文件名
             */
            String fileName = file.getName();
            /**
             * 4、设置响应类型 application/force-download表示 应用程序强制下载
             * 4.1、当你在响应类型为application/octet-stream情况下使用了这个头信息的话，那就意味着你不想直接显示内容，而是弹出一个"文件下载"的对话框，接下来就是由你来决定"打开"还是"保存" 了。
             */
            response.setContentType("application/octet-stream");
            /**
             * 5、设置响应头 这个 attachment 意思就是附件，就是说如果是浏览器能直接解析的文件，比如图片，你也老老实实以附件形式给我下载，别给我直接解析
             * 5.1、Content-disposition 是 MIME 协议的扩展，MIME 协议指示 MIME 用户代理如何显示附加的文件。
             * 5.2、Content-disposition其实可以控制用户请求所得的内容存为一个文件的时候提供一个默认的文件名，文件直接在浏览器上显示或者在访问时弹出文件下载对话框。
             * 格式说明：
             * content-disposition = "Content-Disposition" ":" disposition-type *( ";" disposition-parm )
             * 字段说明：
             * Content-Disposition为属性名
             * disposition-type是以什么方式下载，如attachment为以附件方式下载
             * disposition-parm为默认保存时的文件名
             * 服务端向客户端游览器发送文件时，如果是浏览器支持的文件类型，一般会默认使用浏览器打开，比如txt、jpg等，会直接在浏览器中显示，如果需要提示用户保存，就要利用Content-Disposition进行一下处理，关键在于一定要加上attachment：
             */
            /**
             * 发送的时候使用URLEncoder.encode编码，接收的时候使用URLDecoder.decode解码，都按指定的编码格式进行编码、解码，可以保证不会出现乱码
             * 主要用来http get请求不能传输中文参数问题。http请求是不接受中文参数的。
             * 这就需要发送方，将中文参数encode，接收方将参数decode，这样接收方就能收到准确的原始字符串了。
             */
            response.addHeader("content-disposition", "attachment;filename=" + URLDecoder.decode(fileName, "utf-8"));

            /**
             * 6、文件流读取 输出
             */
            byte[] buffer = new byte[2048];
            //字节输入流
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            //字节输出流
            ServletOutputStream os = response.getOutputStream();
            int i = 0;
            while ((i = bis.read(buffer)) != -1) {
                os.write(buffer, 0, i);
            }
            if (os != null) os.close();
            if (bis != null) bis.close();
        } catch (IOException e) {
            retFileExists(response);
        }
    }

    /**
     * 响应页面信息
     *
     * @param response
     */
    private void retFileExists(HttpServletResponse response) {
        PrintWriter writer = null;
        try {
            response.setContentType("application/text;utf-8");
            response.setCharacterEncoding("utf-8");
            writer = response.getWriter();
            writer.print("文件不存在");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
    }

    /**
     * 删除班级
     *
     * @param classNumber 班级编号
     * @return
     */
    @PostMapping("/deleteClass")
    private boolean deleteClass(String classNumber) {
        try {
            return adminMapper.delClassManageInfo(classNumber);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 创建班级
     *
     * @param classArr 数组对象
     * @return 创建成功true/false
     */
    @PostMapping("/createClass")
    private boolean createClass(String classArr) {
        try{
            JSONObject jsonObject = JSONObject.parseObject(classArr);
            adminMapper.addClassManageInfo(jsonObject.getString("classNumber"), jsonObject.getString("className"));
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
