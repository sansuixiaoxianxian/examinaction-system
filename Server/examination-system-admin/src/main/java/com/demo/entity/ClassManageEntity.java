package com.demo.entity;

public class ClassManageEntity {
    private String className;
    private String classNumber;

    public ClassManageEntity() {
    }

    public ClassManageEntity(String className, String classNumber) {
        this.className = className;
        this.classNumber = classNumber;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    @Override
    public String toString() {
        return "ClassManageEntity{" +
                "className='" + className + '\'' +
                ", classNumber='" + classNumber + '\'' +
                '}';
    }
}
