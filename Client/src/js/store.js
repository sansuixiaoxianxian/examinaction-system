import Vue from 'vue'
import Vuex from 'vuex'
// uex-along - 持久化存储 state 的 vuex 扩展 常用于刷新网页后自动恢复 state
import VuexAlong from 'vuex-along'
Vue.use(Vuex)
const store = new Vuex.Store({
    state:{// 要设置的全局访问的state对象
        // 要设置的初始属性值
        userName:"",// 用户姓名
        userNumber:"",// 用户账号
        userType: "",// 用户类型
        addQuestions:"", // 添加试题班级信息
        questionsList:[], // 考试试题信息
        startExaminationNumber:"", // 开始考试题库号
        endTime:"", // 答题结束时间
        effectiveTime:"", // 经过计算后的 距离考试结束时间
        // -------------echerts数据---------------
        joinNumber:[], // [参考人数,缺考人数]
        classAverage: "", // 班级平均分 
        fractionArray:[], // 各分数段人数
    },
    mutations:{ // 修改全局访问对象state 访问方法：this.$store.commit(store中的方法名，传入的值)
        setUserName(state, value){
            state.userName = value;
        },
        setUserNumber(state, value){
            state.userNumber = value;
        },
        setUserType(state, value){
            state.userType = value;
        },
        setAddQuestions(state, value){
            state.addQuestions = value;
        },
        setQuestionsList(state, value){
            state.questionsList = value;
        },
        setStartExaminationNumber(state, value){
            state.startExaminationNumber = value;
        },
        setEndTime(state, value){
            state.endTime = value;
        },
        setEffectiveTime(state, value){
            state.setEffectiveTime = value;
        },
        setJoinNumber(state, value){
            state.joinNumber = value;
        },
        setClassAverage(state, value){
            state.classAverage = value;
        },
        setFractionArray(state, value){
            state.fractionArray = value;
        }
    },
    getters:{ // 实时监听state值的变化(最新状态) 访问方法：this.$store.getters.getUserName
        getUserName(state){
            return state.userName;
        },
        getUserNumber(state){
            return state.userNumber;
        },
        getUserType(state){
            return state.userType
        },
        getAddQuestions(state){
            return state.addQuestions;
        },
        getQuestionsList(state){
            return state.questionsList;
        },
        getStartExaminationNumber(state){
            return state.startExaminationNumber;
        },
        getEndTime(state){
            return state.endTime;
        },
        getEffectiveTime(state){
            return state.setEffectiveTime;
        },
        getJoinNumber(state){
            return state.joinNumber;
        },
        getClassAverage(state){
            return state.classAverage;
        },
        getFractionArray(state){
            return state.fractionArray;
        }
    },
    plugins:[VuexAlong({name: "vuex-along",})],
})

export default store;
