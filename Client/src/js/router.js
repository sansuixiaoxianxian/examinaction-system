/*
 * @Author: 小花
 * @Date: 2021-09-15 15:07:44
 * @LastEditors: 小花
 * @LastEditTime: 2022-01-17 13:28:11
 * @Description: 
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import store from './store'

import login from '../components/login/login.vue';
import teacher from '../components/teacher/teacher.vue'
import student from '../components/student/student.vue'
import coursesClass from '../components/teacher/coursesClass.vue'
import createCoursesClass from '../components/teacher/createCoursesClass.vue'
import addExaminationQuestions from '../components/teacher/addExaminationQuestions.vue'
import viewPreview from '../components/teacher/viewPreview.vue'
import checkStudentResult from '../components/teacher/checkStudentResult.vue'
import myExamination from '../components/student/myExamination.vue'
import startExamination from '../components/student/startExamination.vue'
import examinationRecord from '../components/student/examinationRecord.vue'
import personalCenter from '../components/student/personalCenter.vue'
import itemAnalysis from '../components/teacher/itemAnalysis.vue'
import signIn from '../components/signIn/signIn.vue'
import wxSigin from '../components/extendProgram/wxSignin/wxSignin.vue'



const router = new VueRouter({
    // mode: 'history',  //去掉url中的# history
    routes: [
        {
            path: "/", component: login, name: "login",
        },
        { path: "/teacher", redirect: "/teacher/coursesClass" },
        {
            path: "/teacher", component: teacher,
            children: [
                { path: "coursesClass", component: coursesClass },
                { path: "createCoursesClass", component: createCoursesClass },
                { path: "checkStudentResult", component: checkStudentResult, name: 'checkStudentResult' },
                { path: "itemAnalysis", component: itemAnalysis, name: "itemAnalysis" },
            ],
        },
        { path: "/addExaminationQuestions", component: addExaminationQuestions },
        { path: "/viewPreview", component: viewPreview },

        { path: "/student", redirect: "/student/myExamination" },
        {
            path: "/student", component: student,
            children: [
                { path: "myExamination", component: myExamination },
                { path: "startExamination", component: startExamination },
                { path: "examinationRecord", component: examinationRecord },
                { path: "personalCenter", component: personalCenter },
                { path: "viewPreview", component: viewPreview },
            ],
        },
        { path: "/signIn", component: signIn, name: 'signIn' },
        { path: "/wxSigin", component: wxSigin, name: 'wxSigin' }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.path == "/") {
        window.clearVuexAlong(true, false);
    } else if (from.path == "/" && (store.state.userName == "" || store.state.userName == "")) {
        next({ path: '/' });
    }
    next();
});

export default router;