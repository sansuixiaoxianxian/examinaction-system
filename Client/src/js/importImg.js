export default {
    img1: require('../images/login_background.jpg'),
    img2: require('../images/floret_logo.ico'),
    img3: require('../images/floret_logo.png'),
    img4: require('../images/complete.png'),
    img5: require('../images/inexam.png'),
    img6: require('../images/waitexam.png'),
    img7: require('../images/more.png'),
    img8: require('../images/signInLogo.png')
}
