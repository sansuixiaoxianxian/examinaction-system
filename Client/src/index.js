import Vue from 'vue'
import app from './app.vue'

import './js/importImg'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.emulateJSON = true // 没有该方法，post提交后端获取不了

Vue.http.options.root = "http://localhost:8080"
Vue.prototype.$ROOT = "http://localhost:8080"


import router from './js/router.js'
import store from './js/store.js'
import 'font-awesome/css/font-awesome.css'

Vue.http.interceptors.push(function (request) {

    let token = sessionStorage.getItem(this.$store.getters.getUserNumber);
    if ((token == null || token == "null") && this.$route.path != "/") {
        this.$router.push("/");
        return false;
    }

    request.headers.set('TOKEN', sessionStorage.getItem(this.$store.getters.getUserNumber));

    return function (response) {
        if (response.bodyText == "401") {
            sessionStorage.removeItem("flag");
            sessionStorage.removeItem("token");
            this.$router.push({
                name: "login",
                params: { status: "信息已过期请重新登录！", type: "warning" }
            });
        }
    }
});

const mv = new Vue({
    el: '#app',
    render: c => c(app),
    router,
    store,
})