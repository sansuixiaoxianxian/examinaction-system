const path = require('path');
// 该插件用于解释单个vue组件使用
const { VueLoaderPlugin } = require('vue-loader')
// 导入在内存中生成页面的插件
const html = require('html-webpack-plugin')


module.exports = {
    mode: 'development', // 开发模式: development  ;  生产模式: production
    entry: {
        index: './src/index.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        clean: true// 输出新文件时，清除旧文件
    },
    devtool: 'inline-source-map',// 让打包后的js文件代码出错后，在浏览器中可以精准定位到原文件错误位置
    module: {
        rules: [
            { test: /\.vue$/, use: 'vue-loader' },
            { test: /\.(sa|sc|c)ss$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
            {
                test: /\.(jpg|png|bmp|jpeg|gif|ico)$/,
                type: 'asset/resource',
                generator: {
                    filename: 'images/[name][ext][query]' // 局部指定输出位置
                }
            },
            {
                test: /\.m?js$/, use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new html({
            // 创建一个在内存中生成html页面的插件
            template: path.join(__dirname, './src/index.html'), // 指定模块页面 
            filename: "index.html",
        }),
    ],
};
